# Purpose : The Assignment folder is contains the assignments of web development course.
# Course  : Full Stack Development on Udemy
# About this course : learn web development - HTML, CSS, JS, Node, and More!
# Trainer : Colt Steele
# Trainee & Author  : Shivesh; shivkumar4680@gmail.com
##############################################################################

Assignments :
1. assignment_1 : Lecture 27, List in html 
2. assignment_2 : Lecture 38, Creating form page (username, email, etc...)

